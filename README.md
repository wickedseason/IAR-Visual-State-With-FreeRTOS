# IAR Visual State With FreeRTOS

#### 项目介绍
自己学习使用的工程，将IAR Visual State生成的代码同FreeRTOS结合使用，在FRDM-K64板上实现

#### 软件使用版本说明
理论上高于下列软件版本相关工程文件可正常打开，低版本有可能无法正常打开
1. IAR for ARM (EWARM) 工程版本 v8.22 	(编译下载用)
2. IAR Visual State 版本 v8.2			
3. MCUXpresso Config Tools 版本 v4.1		(BSP设置)


初步功能已完成，只创建了一个任务对VS工程进行处理，通过定时器中断发送事件