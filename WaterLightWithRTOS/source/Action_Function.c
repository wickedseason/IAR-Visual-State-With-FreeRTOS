#include "SystemLight.h"
#include "System_Init.h"

void led_green_off (void)
{
    LED_GREEN_OFF();
}
void led_green_on (void)
{
    LED_GREEN_ON();
}
void led_red_off (void)
{
    LED_RED_OFF();
}
void led_red_on (void)
{
    LED_RED_ON();
}
void led_yellow_off (void)
{
    LED_YELLOW_OFF();
}
void led_yellow_on (void)
{
    LED_YELLOW_ON();
}